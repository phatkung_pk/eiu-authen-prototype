<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogSendData extends Model
{
    protected $table = "log_send_from_hospital";
}
