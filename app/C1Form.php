<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class C1Form extends Model
{
    //
    protected $table = "c1form";
    protected $fillable = ['table_name','screen_id','staffname','intdd','intmm','intyyyy','age1','age2','thai_ID','live',
    'readwrite','attend','healthy','anymed','breastfeed','pregnant','planpreg','prevenroll',
    'allergyegg','hisparalysis','pastfluvac','reacfluvac','reacfluvac_spec','anyvac','planvac','hcondition','hriskpop',
    'steroid','experiment','hisflu','hisasthma','hisguillain','infect','alcohol','participate','consent','consent_no','id13','sid'];
    //protected $hidden = ['created_at','updated_at'];
}
