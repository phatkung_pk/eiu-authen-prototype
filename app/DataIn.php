<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataIn extends Model
{
    //
    protected $table = "data_in";
    protected $fillable = ['id','text'];
    protected $hidden = ['created_at','updated_at'];
}
