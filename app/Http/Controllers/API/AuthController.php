<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use JWTAuth;
use Validator;
use App\Apps;
use App\Permissions;

class AuthController extends Controller
{
    /**
     * API Login, on success return JWT Auth token
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        //$credentials = $request->only('email', 'password','apps_name');
        $credentials = $request->only('username', 'password');
        $rules = [
            'username' => 'required',
            'password' => 'required',
        ];

        //dd($request->apps_name);
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->messages()
            ]);
        }

        try {
            // Attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'We can`t find an account with this credentials.'
                ], 401);
            }
        } catch (JWTException $e) {
            // Something went wrong with JWT Auth.
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to login, please try again.'
            ], 500);
        }
        //Get App URL with JWT

        // if((isset($token)) && (isset($request->apps_name))){
        //   $apps = Apps::where('apps_name', $request->apps_name)->first();
        //   $url = $apps->apps_url;
        // }else{
        //   return response()->json([
        //       'status' => 'error',
        //       'message' => 'We can`t find apps name.'
        //   ], 401);
        // }
        // dd($apps->apps_url);
        // All good so return the token
        return response()->json([
            'status' => 'success',
            'data'=> [
                'token' => $token,
                //'url' => $url
                // You can add more details here as per you requirment.
            ]
        ]);
    }
    /**
     * Logout
     * Invalidate the token. User have to relogin to get a new token.
     * @param Request $request 'header'
     */
    public function logout(Request $request)
    {
        // Get JWT Token from the request header key "Authorization"
        $token = $request->header('Authorization');
        // Invalidate the token
        try {
            JWTAuth::invalidate($token);
            return response()->json([
                'status' => 'success',
                'message'=> "User successfully logged out."
            ]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
              'status' => 'error',
              'message' => 'Failed to logout, please try again.'
            ], 500);
        }
    }
    /**
    * Get User Detail from token
    **/
    public function getAuthenticatedUser(Request $request)
    {
                    try {

                            if (! $user = JWTAuth::parseToken()->authenticate()) {
                                    return response()->json(['user_not_found'], 404);
                            }

                    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                            return response()->json(['token_expired'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                            return response()->json(['token_invalid'], $e->getStatusCode());

                    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                            return response()->json(['token_absent'], $e->getStatusCode());

                    }
                    //Get URL APP Request
                    if(!empty($request->apps_name)){
                      $apps = Apps::where('apps_name', $request->apps_name)->first();
                      if(empty($apps)){
                        return response()->json([
                            'status' => 'error',
                            'message' => 'We can`t find apps name'
                        ], 401);
                      }else{
                        $url = ['url' => $apps->apps_url, 'apps_name' => $apps->apps_name];
                        $apps_id = $apps->id;
                      }

                    }else{
                      return response()->json([
                          'status' => 'error',
                          'message' => 'We can`t find apps name'
                      ], 401);
                    }

                    //GET Permissions APP Request
                    if(!empty($user->id)){
                      //$permissions = Permissions::groupBy('access_role')->where('user_id',$user->id)->where('apps_id',$apps_id)->get();
                      $roles = DB::table('permissions')->select('access_role')->where('user_id',$user->id)->where('apps_id',$apps_id)->get()->toArray();
                    }
                    return response()->json(compact(['user','url','roles']));
  }
  public function GetAppList(Request $request){
                  try {
                          if (! $user = JWTAuth::parseToken()->authenticate()) {
                                  return response()->json(['user_not_found'], 404);
                      }
                  } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                          return response()->json(['token_expired'], $e->getStatusCode());
                  } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                          return response()->json(['token_invalid'], $e->getStatusCode());
                  } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                          return response()->json(['token_absent'], $e->getStatusCode());
                  }
                  //List App
                  //Check UserID
                  if(empty($user->id)) return response()->json(['user_not_found'], 404);
                  $app_list = DB::table('apps')->select('apps.apps_name','apps.apps_url')
                              ->leftJoin('permissions', 'apps.id', '=', 'permissions.id')
                              ->where('permissions.user_id',$user->id)->get();
                            if(empty($app_list)){
                              return response()->json([
                                  'status' => 'error',
                                  'message' => 'We can`t find Permissions App For List Name'
                                ], 401);
                            }else{
                              return response()->json(compact(['user','app_list']));
                            }
  }

}
