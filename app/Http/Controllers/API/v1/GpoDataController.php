<?php
namespace App\Http\Controllers\API\v1;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use JWTAuth;
use Validator;
//use App\DataIn;
//use App\C1Form;
use App\LogSendData;
use Carbon\Carbon;

class GpoDataController extends Controller
{
    /**
     * API Login, on success return JWT Auth token
     * @param Request $request
     * @return JsonResponse
     */
     public function __construct()
     {
         $this->date_now = Carbon::now();
     }
    public function Post_Data(Request $request){

      $table_name = isset($request->table_name) ? trim($request->table_name) : "";

      //dd(date('Y-m-d H:i:s'));
      $date_times = array('created_at' => $this->date_now,'updated_at' => $this->date_now);
      $data = DB::table($table_name)->insert($request->all()+$date_times);

      if($data===true){
        return response()->json([
            'status' => 'Success',
        ]);
      }else{
        return response()->json([
            'status' => 'Failed',
        ]);
      }

    }

    public function Check_Data_From_Hospital(Request $request){
          $table_name = isset($request->table_name) ? trim($request->table_name) : "";
          $total = isset($request->total) ? trim($request->total) : "";
          try{
              if((!empty($table_name)) && (!empty($total))){
                  $log = new LogSendData;
                  $log->table_name = $table_name;
                  $log->total_record = $total;
                  $result = $log->save();
                  if($result){
                    return response()->json([
                        'status' => 'Success',
                        'total' => (int)$total
                    ]);
                  }else{
                    return response()->json([
                        'status' => 'Failed'
                    ]);
                  }
              }else{
                return response()->json([
                    'status' => 'Failed'
                ]);
              }
          }catch (\Illuminate\Database\QueryException $e) {
              return response()->json([
                  'status' => 'Failed'
              ]);
          }
    }

    public function Check_Data_From_Server(Request $request){
          $table_name = isset($request->table_name) ? trim($request->table_name) : "";
          try{
            $server_count = \DB::table($table_name)->count();
            if($server_count>0){
              return response()->json([
                  'status' => 'Success',
                  'total' => (int)$server_count
              ]);
            }else{
              return response()->json([
                  'status' => 'Failed',
                  'total' => 0
              ]);
            }
          }catch (\Illuminate\Database\QueryException $e) {
              return response()->json([
                  'status' => 'Failed',
                  'message' => 'Table Not Found'
              ]);
          }

    }

}
