<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//Version 1.0
Route::prefix('v1')->group(function () {
  Route::post('login', 'API\AuthController@login');
  Route::middleware('jwt.auth')->group(function(){
      //Route::get('user', 'API\AuthController@getAuthenticatedUser');
      Route::get('logout', 'API\AuthController@logout');
  });
  Route::group(['middleware' => ['jwt.verify']], function() {
          Route::get('user', 'API\AuthController@getAuthenticatedUser');
      });
  //Send Data From Hosital Site
  Route::post('Send-data', 'API\v1\GpoDataController@Post_Data');
  //Check Data Sent From Hosital Site
  Route::post('check-data-from-hospital', 'API\v1\GpoDataController@Check_Data_From_Hospital');
  //Check Data Sent from Server
  Route::get('check-data-from-server', 'API\v1\GpoDataController@Check_Data_From_Server');
});
